import {buildWebpackConfig} from "./config/buildConfig/buildWebpackConfig";
import path from 'path'
import webpack from "webpack";
import {BuildEnv} from "./config/buildConfig/types/config";


export  default  (env: BuildEnv): webpack.Configuration => {
    const mode = env.mode || 'development'
    const PORT = env.port || 3000
    const isDev = mode == 'development'

    return buildWebpackConfig({
        mode,
        paths: {
            entry: path.resolve(__dirname, 'src', 'index.tsx'),
            build: path.resolve(__dirname, 'build'),
            html: path.resolve(__dirname, 'public', 'index.html')
        },
        port: PORT,
        isDev,
    })
};