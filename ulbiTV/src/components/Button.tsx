
import React from "react";
import classes from './Button.module.scss'

export const Button = () => {
    return (
        <button>
            <span className={classes.textStyle}>
                SEND
            </span>
        </button>
    )
}